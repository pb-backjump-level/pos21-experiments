# Experiments on Improving the Backjump Level in PB Solvers

This repository provides the experimental results of the different strategies
that have been designed to try to improve the backjump level in PB solvers.

All experiments have been executed on a cluster of computers equipped with quadcore
bi-processors Intel XEON X5550 (2.66 GHz, 8 MB cache) and 32 GB of memory.
The nodes of this cluster run Linux CentOS 7 (x86_64), and use GCC 4.8.5 for
C/C++-based programs and the JDK 11.0.1 for Java-based programs.

The analyses of our experiments can be accessed from the following table of
contents.
All the analyses are powered by [*Metrics*](https://metrics.readthedocs.io/) and
provided as Jupyter Notebooks.
They may thus be reproduced by cloning this repository and running the notebooks again
with the [*version 1.0.3 Metrics*](https://pypi.org/project/crillab-metrics/1.0.3/).
Note that, to achieve reproducibility, we also provide the (JSON-serialized) data
we collected during our experiments.

> **Important note:**
> Deserialization issues may occur if your version of *Metrics* is below 1.0.3.
> Please, make sure that you use this version before running the analyses.

+ [Analyze until backjump level and weaken any literal](until-bjlevel.ipynb)
+ [Analyze until backjump level (unless it is the top level) and weaken any literal](until-top-level.ipynb)
+ [Analyze until backjump level (unless it is a "high" level) and weaken any literal](until-high-level.ipynb)
+ [Analyze until backjump level and never weaken literals](until-bjlevel-never.ipynb)
+ [Analyze until backjump level and weaken literal in order](until-bjlevel-ordered.ipynb)
